import axios from "axios";
import TaskConstant from "../constants/TaskConstants";
const apiUrl = "http://localhost:3600/tasks";

export const createTask = ({ title, description, dueDate }) => {
  return dispatch => {
    return axios
      .post(`${apiUrl}/`, { title, description, dueDate })
      .then(response => {
        if (response.status === 200) {
          dispatch(createTaskSuccess(response));
        } else {
          dispatch(createTaskFailure(response));
        }
      })
      .catch(error => {
        throw error;
      });
  };
};

export const createTaskSuccess = data => {
  return {
    type: TaskConstant.CREATE_REQUEST_SUCCESS,
    payload: {
      ...data
    }
  };
};
export const createTaskFailure = data => {
  return {
    type: TaskConstant.CREATE_REQUEST_FAILED,
    payload: {
      ...data
    }
  };
};
export const deleteTaskSuccess = response => {
  return {
    type: TaskConstant.DELETE_REQUEST_SUCCESS,
    payload: {
      ...response
    }
  };
};
export const deleteTaskFailure = response => {
  return {
    type: TaskConstant.DELETE_REQUEST_FAILED,
    payload: {
      ...response
    }
  };
};
export const deleteTask = id => {
  return dispatch => {
    return axios
      .delete(`${apiUrl}/${id}`)
      .then(response => {
        if (response.status === 200) {
          dispatch(deleteTaskSuccess(response));
        } else {
          dispatch(deleteTaskFailure(response));
        }
      })
      .catch(error => {
        throw error;
      });
  };
};
export const fetchTask = id => {
  console.log(id);
  return dispatch => {
    return axios
      .get(`${apiUrl}/${id}`)
      .then(response => {
        console.log("ewrewre");
        console.log(response);
        dispatch(fetchTaskSuccess(response));
      })
      .catch(error => {
        throw error;
      });
  };
};
export const fetchTaskSuccess = response => {
  console.log(response);
  return {
    type: TaskConstant.FETCH_BY_ID_REQUEST_SUCCESS,
    payload: {
      ...response.data
    }
  };
};
export const handleChange = (key, value) => {
  return dispatch => {
    return dispatch(handleChangeSuccess({ [key]: value }));
  };
};
export const handleChangeSuccess = obj => {
  return {
    type: TaskConstant.HANDLE_INPUT_CHANGE,
    payload: {
      ...obj
    }
  };
};
export const fetchTaskFailure = response => {
  return {
    type: TaskConstant.FETCH_BY_ID_REQUEST_FAILED,
    payload: {
      ...response
    }
  };
};
export const fetchTasks = tasks => {
  //console.log(tasks);
  return {
    type: TaskConstant.FETCH_ALL_REQUEST_SUCCESS,
    payload: tasks.data
  };
};

export const fetchAllTasks = (limit = 10, page = 0) => {
  return dispatch => {
    return axios
      .get(`${apiUrl}`, {
        params: {
          limit,
          page
        }
      })
      .then(response => {
        dispatch(fetchTasks(response));
      })
      .catch(error => {
        throw error;
      });
  };
};
export const updateTask = ({ _id, title, description, dueDate }) => {
  return dispatch => {
    return axios
      .put(`${apiUrl}/${_id}`, { title, description, dueDate })
      .then(response => {
        if (response.status === 200) {
          dispatch(updateTaskSuccess(response));
        } else {
          dispatch(updateTaskFailure(response));
        }
      })
      .catch(error => {
        throw error;
      });
  };
};

export const updateTaskSuccess = data => {
  return {
    type: TaskConstant.EDIT_REQUEST_SUCCESS,
    payload: {
      ...data
    }
  };
};
export const updateTaskFailure = data => {
  return {
    type: TaskConstant.EDIT_REQUEST_FAILED,
    payload: {
      ...data
    }
  };
};
