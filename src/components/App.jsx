import React, { Component } from "react";
import NavBar from "./NavBar/NavBar";
import TaskForm from "./Tasks/TaskForm";
import TaskList from "./Tasks/TaskList";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import EditForm from "./Tasks/EditForm";
class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <React.Fragment>
          <NavBar />
          <Switch>
            <Route exact path="/" component={TaskList} />
            <Route path="/add-task" component={TaskForm} />
            <Route path="/tasks/:task_id" component={EditForm} />
          </Switch>
        </React.Fragment>
      </BrowserRouter>
    );
  }
}

export default App;
