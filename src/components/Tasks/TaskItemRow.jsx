import React, { Component } from "react";
import { Delete, Edit } from "@material-ui/icons";
import { Link } from "react-router-dom";

class TaskItemRow extends Component {
  state = {};
  confirmDelete = id => {
    //console.log(id);
    if (window.confirm("Are you sure you wish to delete this item?"))
      this.props.onDelete(id);
  };
  render() {
    //console.log(this.props);
    const { isLoading, taskList } = this.props;
    let trItem;
    if (isLoading) {
      trItem = (
        <tr key="loading">
          <td colSpan="3" className="center">
            Loading Data...
          </td>
        </tr>
      );
    } else if (typeof taskList === undefined) {
      trItem = (
        <tr key="no-data-found">
          <td colSpan="3" className="center">
            No Data Found!
          </td>
        </tr>
      );
    } else if (taskList.length === 0) {
      trItem = (
        <tr key="no-data-found">
          <td colSpan="3" className="center">
            No Data Found!
          </td>
        </tr>
      );
    } else {
      let rowIndex = 1;
      trItem = taskList.map(task => {
        return (
          <tr key={task._id}>
            <td>{rowIndex++}</td>
            <td>
              <Link to={"/tasks/" + task._id}>{task.title}</Link>
            </td>
            <td>{new Date(Date.parse(task.dueDate)).toLocaleDateString()}</td>
            <td>
              <Link to={"/tasks/" + task._id}>
                <Edit />
              </Link>
              <Delete
                className="red-text text-darken-2"
                onClick={() => {
                  this.confirmDelete(task._id);
                }}
              />
            </td>
          </tr>
        );
      });
    }
    return trItem;
  }
}

export default TaskItemRow;
