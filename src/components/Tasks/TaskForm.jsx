import React, { Component } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { connect } from "react-redux";
import { createTask } from "../../_actions/index";

class TaskForm extends Component {
  state = {
    formTitle: "Add Task",
    _id: "",
    title: "",
    description: "",
    dueDate: new Date()
  };
  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });

    //console.log(this.state);
  };
  handleDateChange = date => {
    this.setState({
      dueDate: date
    });

    //console.log(this.state);
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.onCreateTask(this.state);
    this.props.history.push(`/`);
  };
  render() {
    const { title, description, dueDate, formTitle } = this.state;
    const selectedDate = Date.parse(dueDate);
    return (
      <div className="container">
        <div className="row">
          <h5 className="center"> {formTitle}</h5>
          <form onSubmit={this.handleSubmit}>
            <div className="row">
              <div className="input-field col s12">
                <input
                  required
                  id="title"
                  type="text"
                  value={title}
                  className="validate"
                  onChange={this.handleChange}
                />
                <label htmlFor="title">Title</label>
              </div>
              <div className="row">
                <div className="input-field col s12">
                  <textarea
                    required
                    id="description"
                    onChange={this.handleChange}
                    className="materialize-textarea validate"
                    value={description}
                  ></textarea>
                  <label htmlFor="description">Description</label>
                </div>
              </div>
              <div className="row">
                <div className="input-field col s12">
                  <DatePicker
                    id="dueDate"
                    required
                    selected={selectedDate}
                    onChange={this.handleDateChange}
                  />
                </div>
              </div>
              <div className="row">
                <button
                  className="btn btn-small btn-primary"
                  onClick={this.handleClick}
                >
                  Add Task
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onCreateTask: (title, description, dueDate) => {
      dispatch(createTask(title, description, dueDate));
    }
  };
};
export default connect(
  null,
  mapDispatchToProps
)(TaskForm);
