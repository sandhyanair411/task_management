import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchAllTasks, deleteTask } from "../../_actions/index";
import TaskItemRow from "./TaskItemRow";
import { CardHeader } from "@material-ui/core";
import ReactPaginate from "react-paginate";
class TaskList extends Component {
  componentDidMount() {
    this.props.getTasks();
  }
  // componentDidUpdate() {
  //   this.props.getTasks();
  // }
  handleDelete = id => {
    // console.log(id);
    this.props.deleteTask(id);
  };
  handlePageClick = data => {
    this.props.getTasks(this.props.limit, data.selected);
  };
  render() {
    console.log(this.props);
    const { data: tasks, isLoading } = this.props;
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <div className="card">
              <CardHeader title="Task List" />
              <div className="card-body">
                <table className="table table-hover">
                  <thead className="thead-dark">
                    <tr>
                      <th>#</th>
                      <th>Title</th>
                      <th>Due Date</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <TaskItemRow
                      key="taskItemRow1"
                      isLoading={isLoading}
                      onDelete={id => {
                        this.handleDelete(id);
                      }}
                      taskList={tasks}
                    />
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colSpan="3">
                        <ReactPaginate
                          pageCount={this.props.totalPages}
                          onPageChange={this.handlePageClick}
                          containerClassName={"pagination"}
                          subContainerClassName={"pages pagination"}
                          activeClassName={"active"}
                        />
                      </td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  console.log("map");
  console.log(state);
  return {
    ...state
  };
};
const mapDispatchToProps = dispatch => {
  return {
    getTasks: (limit = 10, page = 0) => dispatch(fetchAllTasks(limit, page)),
    deleteTask: id => dispatch(deleteTask(id))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TaskList);
