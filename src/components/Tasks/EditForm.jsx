import React, { Component } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { connect } from "react-redux";
import { fetchTask, handleChange, updateTask } from "../../_actions/index";

class EditForm extends Component {
  componentDidMount() {
    const { task_id } = this.props.match.params;
    if (task_id) {
      this.setState({ formTitle: "Edit Task" });
      this.props.onFetchTask(task_id);
    } else {
      this.props.history.push(`/`);
    }
  }
  handleDateChange = date => {
    this.props.onHandleChange("dueDate", date);

    //console.log(this.state);
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.onUpdateTask(this.props);
    this.props.history.push(`/`);
  };
  render() {
    const { title, description, dueDate } = this.props;
    const selectedDate = Date.parse(dueDate);
    return (
      <div className="container">
        <div className="row">
          <h5 className="center">Edit Task</h5>
          <form onSubmit={this.handleSubmit}>
            <div className="row">
              <div className="input-field col s12">
                <input
                  required
                  id="title"
                  type="text"
                  value={title}
                  className="validate"
                  onChange={e => {
                    this.props.onHandleChange(e.target.id, e.target.value);
                  }}
                />
                <label htmlFor="title">Title</label>
              </div>
              <div className="row">
                <div className="input-field col s12">
                  <textarea
                    required
                    id="description"
                    onChange={e => {
                      this.props.onHandleChange(e.target.id, e.target.value);
                    }}
                    className="materialize-textarea validate"
                    value={description}
                  ></textarea>
                  <label htmlFor="description">Description</label>
                </div>
              </div>
              <div className="row">
                <div className="input-field col s12">
                  <DatePicker
                    id="dueDate"
                    required
                    selected={selectedDate}
                    onChange={this.handleDateChange}
                  />
                </div>
              </div>
              <div className="row">
                <button
                  className="btn btn-small btn-primary"
                  onClick={this.handleSubmit}
                >
                  Update Task
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return { ...state };
};
const mapDispatchToProps = dispatch => {
  return {
    onFetchTask: id => {
      dispatch(fetchTask(id));
    },
    onHandleChange: (key, value) => {
      dispatch(handleChange(key, value));
    },
    onUpdateTask: props => {
      dispatch(updateTask(props));
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditForm);
