import TaskConstant from "../constants/TaskConstants";
const initState = {
  isLoading: true,
  limit: 10,
  data: [],
  _id: "",
  title: "",
  description: "",
  dueDate: new Date()
};
const rootReducer = (state = initState, action) => {
  console.log(action.type);
  console.log(state);
  console.log(action);
  switch (action.type) {
    case TaskConstant.CREATE_REQUEST_SUCCESS:
      return {
        ...state,
        data: [{ ...action.payload.data.data }, ...state.data].slice(
          0,
          state.limit
        )
      };
    case TaskConstant.CREATE_REQUEST_FAILED:
      return { ...state };
    case TaskConstant.DELETE_REQUEST_SUCCESS:
      // console.log("bas yar");
      // console.log(state);
      // console.log(action);
      return {
        ...state,
        data: state.data.filter(task => task._id !== action.payload.data._id)
      };
    case TaskConstant.FETCH_BY_ID_REQUEST_SUCCESS:
      return { ...state, ...action.payload.data };
    case TaskConstant.FETCH_ALL_REQUEST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        data: [...action.payload.data]
      };
    case TaskConstant.HANDLE_INPUT_CHANGE:
      return {
        ...state,
        ...action.payload
      };
    default:
      return state;
  }
};
export default rootReducer;
